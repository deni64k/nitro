#include <iostream>

#include "nitro.hxx"

int main(int /*argc*/, char ** /*argv*/) {
  // Optimize C++-style standard input and output streams sacrificing C-style ones.
  // It is done on purpose for large JSON input.
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  auto rects = parseInput(std::cin);

  std::cout << "Input\n";
  for (std::size_t i = 0; i < rects.size(); ++i) {
    auto &rect = rects[i];
    rect.ids.emplace_back(i + 1);
    std::cout << '\t' << rect.ids[0]
              << ": Rectangle at (" << rect.x << ',' << rect.y << ')'
              << ", w=" << rect.width << ", h=" << rect.height
              << '.' << std::endl;
  }

  auto const intersections = findIntersections(rects);

  std::cout << "Intersections\n";
  for (auto const &inter : intersections) {
    std::cout << "\tBetween rectangle " << formatIds(inter.ids)
              << " at (" << inter.x << ',' << inter.y << ')'
              << ", w=" << inter.width << ", h=" << inter.height
              << '.' << std::endl;
  }
}
