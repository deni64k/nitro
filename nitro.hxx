#pragma once

#include <vector>
#include <string>

#include "json.hxx"

#define DEBUG(x) x
// #define DEBUG(x)

struct Rect {
  std::vector<int> ids;
  int x, y, width, height;
};

inline
void from_json(nlohmann::json const &j, Rect &r) {
  r.x      = j.at("x").get<int>();
  r.y      = j.at("y").get<int>();
  r.width  = j.at("w").get<int>();
  r.height = j.at("h").get<int>();
}

std::vector<Rect> parseInput(std::istream &);

std::string formatIds(std::vector<int> const &);
std::vector<int> mergeIds(std::vector<int> const &, std::vector<int> const &);

bool doesIntersectionExist(Rect const &, Rect const &);
Rect getIntersection(Rect const &, Rect const &);
std::vector<Rect> findIntersections(std::vector<Rect>);
