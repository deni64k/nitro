# What's here?

Hi there! Here is a test assessment and a very short README file.

# How to run it?

Check out the repository and use Makefile. Here is how:

```sh
git clone https://negval@bitbucket.org/negval/nitro.git
cd nitro
make run
```

The `run` target will run a compiled binary, `nitro`, with input from `in.json`.

# Do you have any tests?

Yes, I used Catch library due to it simplicity to integrate.

To run tests use `test` target.

```sh
make test
```

# Is there anything else I should know?

Although README file is short, I supplied sources with comments. The algorithm itself is straightforward with a few optimisations to prevent from unnecessary iterations.

Here are my online profiles:

* HackerRank: https://www.hackerrank.com/d_sukhonin

* Github: https://github.com/neglectedvalue

* Bitbucket: https://bitbucket.org/negval

Have a nice day.
