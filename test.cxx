#include <sstream>

#include "nitro.hxx"

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hxx"

TEST_CASE("JSON input must be parsed", "[helpers]") {
  std::stringstream ss;

  SECTION("handling an empty input") {
    ss << R"({})";

    REQUIRE_THROWS(parseInput(ss));
  }

  SECTION("handling a non-array input") {
    ss << R"({"rects": {}})";

    REQUIRE_THROWS(parseInput(ss));
  }

  SECTION("handling an empty input array") {
    ss << R"({"rects": []})";
    auto const &parsed = parseInput(ss);

    REQUIRE(parsed.empty());
  }

  SECTION("handling non-empty input array") {
    ss << R"({"rects": [{"x": 1, "y": 2, "w": 11, "h": 22}]})";
    auto const &parsed = parseInput(ss);

    REQUIRE(parsed.size() == 1);
    REQUIRE(parsed[0].x == 1);
    REQUIRE(parsed[0].y == 2);
    REQUIRE(parsed[0].width == 11);
    REQUIRE(parsed[0].height == 22);
  }
}

TEST_CASE("formatIds must format a list of numbers", "[helpers]") {
  SECTION("handling an empty input") {
    std::vector<int> nums = {};

    REQUIRE(formatIds(nums) == "");
  }

  SECTION("handling a single element") {
    std::vector<int> nums = {42};

    REQUIRE(formatIds(nums) == "42");
  }

  SECTION("handling two elements") {
    std::vector<int> nums = {42, 43};

    REQUIRE(formatIds(nums) == "42 and 43");
  }

  SECTION("handling three and more elements") {
    std::vector<int> nums = {42, 43, 44};

    REQUIRE(formatIds(nums) == "42, 43 and 44");
  }
}

TEST_CASE("mergeIds must merge two lists of ids", "[helpers]") {
  SECTION("handling an empty input") {
    auto const &merged = mergeIds({}, {});

    REQUIRE(merged.empty());
  }

  SECTION("handling a single element") {
    auto const &merged = mergeIds({1}, {});

    REQUIRE(merged.size() == 1);
    REQUIRE(merged[0] == 1);
  }

  SECTION("handling two elements") {
    auto const &merged = mergeIds({3}, {1});

    REQUIRE(merged.size() == 2);
    REQUIRE(merged[0] == 1);
    REQUIRE(merged[1] == 3);
  }

  SECTION("handling common elements") {
    auto const &merged = mergeIds({3, 4, 5}, {1, 2, 3, 4});

    REQUIRE(merged.size() == 5);
    REQUIRE(merged[0] == 1);
    REQUIRE(merged[1] == 2);
    REQUIRE(merged[2] == 3);
    REQUIRE(merged[3] == 4);
    REQUIRE(merged[4] == 5);
  }
}

TEST_CASE("doesIntersectionExist must find determine if the intersection exists",
          "[intersection]") {
  SECTION("handling non-intersecting rectangles") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 100, 100, 100, 100};

    REQUIRE_FALSE(doesIntersectionExist(rectA, rectB));
  }

  SECTION("handling two rectangles with the same parameters") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 0, 0, 100, 100};

    REQUIRE(doesIntersectionExist(rectA, rectB));
  }

  SECTION("handling two partly intersecting rectangles") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 50, 50, 100, 100};

    REQUIRE(doesIntersectionExist(rectA, rectB));
  }
}

TEST_CASE("getIntersection must find the intersection of two rectangles",
          "[intersection]") {
  SECTION("handling two rectangles with the same parameters") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 0, 0, 100, 100};
    auto const &inter = getIntersection(rectA, rectB);

    REQUIRE(inter.ids.size() == 2);
    REQUIRE(inter.ids[0] == 1);
    REQUIRE(inter.ids[1] == 2);
    REQUIRE(inter.x      == 0);
    REQUIRE(inter.y      == 0);
    REQUIRE(inter.width  == 100);
    REQUIRE(inter.height == 100);
  }

  SECTION("handling two partly intersecting rectangles") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 50, 50, 100, 100};
    auto const &inter = getIntersection(rectA, rectB);

    REQUIRE(inter.ids.size() == 2);
    REQUIRE(inter.ids[0] == 1);
    REQUIRE(inter.ids[1] == 2);
    REQUIRE(inter.x      == 50);
    REQUIRE(inter.y      == 50);
    REQUIRE(inter.width  == 50);
    REQUIRE(inter.height == 50);
  }

  SECTION("handling two fully overlapped intersecting rectangles") {
    Rect const rectA{{1}, 0, 0, 100, 100};
    Rect const rectB{{2}, 25, 25, 50, 50};
    auto const &inter = getIntersection(rectA, rectB);

    REQUIRE(inter.ids.size() == 2);
    REQUIRE(inter.ids[0] == 1);
    REQUIRE(inter.ids[1] == 2);
    REQUIRE(inter.x      == 25);
    REQUIRE(inter.y      == 25);
    REQUIRE(inter.width  == 50);
    REQUIRE(inter.height == 50);
  }
}

TEST_CASE("findIntersections must find all intersections among rectangles",
          "[intersection]") {
  SECTION("handling non-intersecting rectangles") {
    std::vector<Rect> const rects{
      {{1},   0,   0, 100, 100},
      {{2}, 100, 100, 100, 100},
      {{3},   0, 100, 100, 100},
      {{4}, 100,   0, 100, 100}
    };
    auto const &result = findIntersections(rects);

    REQUIRE(result.empty());
  }

  SECTION("handling rectangles with the same parameters") {
    std::vector<Rect> const rects{
      {{1}, 0, 0, 100, 100},
      {{2}, 0, 0, 100, 100},
      {{3}, 0, 0, 100, 100},
    };
    auto const &result = findIntersections(rects);

    REQUIRE(result.size()    == 4);
    REQUIRE(result[0].ids    == std::vector<int>({1, 2}));
    REQUIRE(result[0].x      == 0);
    REQUIRE(result[0].y      == 0);
    REQUIRE(result[0].width  == 100);
    REQUIRE(result[0].height == 100);
    REQUIRE(result[1].ids    == std::vector<int>({1, 3}));
    REQUIRE(result[1].x      == 0);
    REQUIRE(result[1].y      == 0);
    REQUIRE(result[1].width  == 100);
    REQUIRE(result[1].height == 100);
    REQUIRE(result[2].ids    == std::vector<int>({2, 3}));
    REQUIRE(result[2].x      == 0);
    REQUIRE(result[2].y      == 0);
    REQUIRE(result[2].width  == 100);
    REQUIRE(result[2].height == 100);
    REQUIRE(result[3].ids    == std::vector<int>({1, 2, 3}));
    REQUIRE(result[3].x      == 0);
    REQUIRE(result[3].y      == 0);
    REQUIRE(result[3].width  == 100);
    REQUIRE(result[3].height == 100);
  }

  SECTION("handling partly intersecting rectangles") {
    std::vector<Rect> const rects{
      {{1},  0, 0, 100, 100},
      {{2}, 50, 0, 100, 100},
      {{3}, 0, 50, 100, 100},
    };
    auto const &result = findIntersections(rects);

    REQUIRE(result.size()    == 4);
    REQUIRE(result[0].ids    == std::vector<int>({1, 2}));
    REQUIRE(result[0].x      == 50);
    REQUIRE(result[0].y      == 0);
    REQUIRE(result[0].width  == 50);
    REQUIRE(result[0].height == 100);
    REQUIRE(result[1].ids    == std::vector<int>({1, 3}));
    REQUIRE(result[1].x      == 0);
    REQUIRE(result[1].y      == 50);
    REQUIRE(result[1].width  == 100);
    REQUIRE(result[1].height == 50);
    REQUIRE(result[2].ids    == std::vector<int>({2, 3}));
    REQUIRE(result[2].x      == 50);
    REQUIRE(result[2].y      == 50);
    REQUIRE(result[2].width  == 50);
    REQUIRE(result[2].height == 50);
    REQUIRE(result[3].ids    == std::vector<int>({1, 2, 3}));
    REQUIRE(result[3].x      == 50);
    REQUIRE(result[3].y      == 50);
    REQUIRE(result[3].width  == 50);
    REQUIRE(result[3].height == 50);
  }

  SECTION("handling fully overlapped rectangles") {
    std::vector<Rect> const rects{
      {{1},  0,  0, 100, 100},
      {{2}, 25, 25,  50,  50},
      {{3}, 50, 50,  10,  10},
    };
    auto const &result = findIntersections(rects);

    REQUIRE(result.size()    == 4);
    REQUIRE(result[0].ids    == std::vector<int>({1, 2}));
    REQUIRE(result[0].x      == 25);
    REQUIRE(result[0].y      == 25);
    REQUIRE(result[0].width  == 50);
    REQUIRE(result[0].height == 50);
    REQUIRE(result[1].ids    == std::vector<int>({1, 3}));
    REQUIRE(result[1].x      == 50);
    REQUIRE(result[1].y      == 50);
    REQUIRE(result[1].width  == 10);
    REQUIRE(result[1].height == 10);
    REQUIRE(result[2].ids    == std::vector<int>({2, 3}));
    REQUIRE(result[2].x      == 50);
    REQUIRE(result[2].y      == 50);
    REQUIRE(result[2].width  == 10);
    REQUIRE(result[2].height == 10);
    REQUIRE(result[3].ids    == std::vector<int>({1, 2, 3}));
    REQUIRE(result[3].x      == 50);
    REQUIRE(result[3].y      == 50);
    REQUIRE(result[3].width  == 10);
    REQUIRE(result[3].height == 10);
  }
}
