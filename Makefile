CXX = g++
CXXFLAGS = -std=c++14 -Wall -Wextra -pedantic -O2
SRC = nitro.cxx solution.cxx
HDRS = nitro.hxx json.hxx
OBJ = $(SRC:.cxx=.o)

.PHONY: run test clean

all: test

.SUFFIXES: .cxx .o
.cxx.o: $(HDRS)
	@g++ -c $(CXXFLAGS) $<

test.o: test.cxx $(HDRS) catch.hxx

nitro: $(OBJ)
	@g++ $(LDFLAGS) -o $@ $^

run: nitro
	./nitro < in.json

test: solution.o test.o
	@g++ $(LDFLAGS) -o $@ $^
	@./test

clean:
	-rm -f *.o nitro test
