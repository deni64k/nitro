#include <algorithm>
#include <iostream>
#include <queue>
#include <sstream>
#include <unordered_set>

#include "nitro.hxx"

// inInterval checks if `x` is between `begin` and `end`.
inline
bool inInterval(int x, int begin, int end) {
  return x > begin && x < end;
}

// parseInput takes a stream `in` as input and extracts found rectangles regarding their
// JSON representation.
std::vector<Rect> parseInput(std::istream &in) {
  nlohmann::json json;
  in >> json;
  
  return json.at("rects");
}

// formatIds takes a vector of `ids` and formats them in the readable way, g.e.
// formatIds([1, 2, 3]) produces "1, 2 and 3"
// NOTE: The oxford comma is not included.
std::string formatIds(std::vector<int> const &numbers) {
  std::stringstream result;
  std::size_t i = 0;

  if (numbers.empty()) {
    return "";
  }

  if (numbers.size() > 0) {
    result << numbers[i++];
  }
  
  while (i < numbers.size() - 1) {
    result << ", " << numbers[i++];
  }

  if (numbers.size() > 1) {
    result << " and " << numbers[i];
  }

  return result.str();
}

// mergeIds takes two vectors of numbers and merge them discarding duplicates.
std::vector<int> mergeIds(std::vector<int> const &idsA, std::vector<int> const &idsB) {
  std::vector<int> idsResult;
  std::size_t i = 0, j = 0;
  while (i < idsA.size() && j < idsB.size()) {
    if (idsA[i] < idsB[j]) {
      idsResult.emplace_back(idsA[i++]);
    } else if (idsA[i] > idsB[j]) {
      idsResult.emplace_back(idsB[j++]);
    } else {
      idsResult.emplace_back(idsA[i++]);
      j++;
    }
  }
  while (i < idsA.size()) {
    idsResult.emplace_back(idsA[i++]);
  }
  while (j < idsB.size()) {
    idsResult.emplace_back(idsB[j++]);
  }
  return idsResult;
}

// HashIds implements a hash key for rectangle IDs.
// This hash hasn't been tested on the fill rate, but looks right to me. negval@
struct HashIds {
  std::size_t operator () (std::vector<int> const &ids) const {
    std::size_t result = 0;
    for (auto const id : ids) {
      result = (result << 1) ^ id;
    }
    return result;
  }
};

// doesIntersectionExist takes two rectangles `rectA` and `rectB` and returns true
// if they intersect, otherwise false.
bool doesIntersectionExist(Rect const &rectA, Rect const &rectB) {
  std::size_t i = 0, j = 0;
  while (i < rectA.ids.size() && j < rectB.ids.size()) {
    if (rectA.ids[i] == rectB.ids[j]) {
      // Common rectangle found; skip
      return false;
    } else if (rectA.ids[i] < rectB.ids[j]) {
      ++i;
    } else {
      ++j;
    }
  }

  return inInterval(rectB.x - rectA.x, -rectB.width, rectA.width) &&
      inInterval(rectB.y - rectA.y, -rectB.height, rectA.height);
}

// getIntersection takes two instersecting rectangles `rectA` and `rectB` and returns
// their intersection.
// NOTE: The input rectangles must have an intersection, otherwise the output won't make sense.
Rect getIntersection(Rect const &rectA, Rect const &rectB) {
  Rect inter;
  
  inter.ids = mergeIds(rectA.ids, rectB.ids);

  inter.x = std::max(rectA.x, rectB.x);
  inter.y = std::max(rectA.y, rectB.y);

  int right  = std::min(rectA.x + rectA.width,  rectB.x + rectB.width);
  int bottom = std::min(rectA.y + rectA.height, rectB.y + rectB.height);
  inter.width  = right - inter.x;
  inter.height = bottom - inter.y;

  return inter;
}

// findIntersections takes a vector of rectangles `rects` and returns all intersections
// among them.
std::vector<Rect> findIntersections(std::vector<Rect> rects) {
  std::vector<Rect> result;

  // A queue to schedule found intersections for seeking further intersections.
  std::queue<Rect> q;

  // A set which is used to check if a rectangle have already been seen (handled).
  std::unordered_set<std::vector<int>, HashIds> seen;

  // Firstly, go over all rectangles and check if against each other.
  // If an intersection encountered, put it into the result vector,
  // and schedule for further checking through the queue `q`.
  // Mark every found intersection as seen.
  for (std::size_t i = 0; i < rects.size(); ++i) {
    for (std::size_t j = i + 1; j < rects.size(); ++j) {
      if (doesIntersectionExist(rects[i], rects[j])) {
        auto const &inter = getIntersection(rects[i], rects[j]);

        q.emplace(inter);
        result.emplace_back(inter);
        // XXX: A micro-optimization to prevent from copying `inter.ids`.
        seen.emplace(std::move(inter.ids));
      }
    }
  }

  // Secondly, handle the queue `q` until it's empty.
  // The iterations here are very similar to the previous ones, but here we also check
  // if we have seen a new intersection, and if so we skip. Otherwise, we schedule it
  // mark it as seen, and put into the result.
  while (!q.empty()) {
    Rect rect = q.front();
    q.pop();
    
    for (std::size_t i = 0; i < rects.size(); ++i) {
      if (doesIntersectionExist(rect, rects[i])) {
        auto const &inter = getIntersection(rect, rects[i]);

        if (seen.count(inter.ids) > 0) {
          continue;
        }

        q.emplace(inter);
        result.emplace_back(inter);
        // XXX: A micro-optimization to prevent from copying `inter.ids`.
        seen.emplace(std::move(inter.ids));
      }
    }

    rects.emplace_back(rect);
  }

  return result;
}
